#!/bin/sh
# 2008 Vitaly Lipatov (Etersoft)
# 2009 Baranov Denis (Etersoft)
# script for install new build in the chroot
# run with version public_rel private_rel
# example: install.sh SQL 1.0.10 eter25 eter18

if [ -n "$4" ] ; then
    PRIVVER=$1
    shift
else PRIVVER=sql
fi

VERSION=$1
PUBREL=$2
PRIVREL=$3

if [ -z "$VERSION" ] || [ -z "$PUBREL" ] || [ -z "$PRIVREL" ] ; then
    echo "Usage example:  install.sh [version] 1.0.10 eter25 eter18"
    echo "    version - SQL, SQLUnique, Network, NetworkLite, NetworkUnique, Local"
    exit 1
fi

ROOTDIR=/net/legacy/$VERSION-$PUBREL-$PRIVREL-$PRIVVER

PROJECTNAME=WINE@Etersoft

# Каталог с деревом пакетов свободного WINE и файлов к нему
WINEPUB_PATH=/var/ftp/pub/Etersoft/$PROJECTNAME

# Каталог с закрытой частью
WINEETER_PATH=/var/ftp/pvt/Etersoft/$PROJECTNAME

SYSTEM=ALTLinux/Sisyphus

DIRPUB="$VERSION-$PUBREL"
DIRPVT="$VERSION-$PRIVREL"

CUR_PATH_PUB=$DIRPUB/WINE/$SYSTEM

if [ $VERSION = "1.0.9" ] ;
then PACKAGENAME_PUB=wine
     RPMLIST="$WINEPUB_PATH/$CUR_PATH_PUB/lib$PACKAGENAME_PUB-$VERSION-*.i586.rpm"
else PACKAGENAME_PUB=wine-etersoft
     RPMLIST=""
fi
PACKAGENAME=wine-etersoft
echo "$PRIVVER"
RPMLIST="$WINEPUB_PATH/$CUR_PATH_PUB/$PACKAGENAME_PUB-$VERSION-*.i586.rpm
	$RPMLIST
        $WINEETER_PATH/$DIRPVT/WINE-$PRIVVER/$SYSTEM/$PACKAGENAME-*-$VERSION-*.i586.rpm"

for i in $RPMLIST ; do
    test -r "$i" && continue
    echo "Package $i is missed"
    exit 1
done

if [ -d $ROOTDIR/var/lib ] ; then
    echo "$ROOTDIR already exists. Check it first."
    exit 1
fi

mkdir -p $ROOTDIR/var/lib/rpm || exit 1
rpm --root $ROOTDIR --initdb || exit 1
rpm --root $ROOTDIR -ivh $RPMLIST --nodeps --noscripts || exit 1

rm -f $VERSION
ln -s $VERSION-$PUBREL-$PRIVREL-$PRIVVER $VERSION
